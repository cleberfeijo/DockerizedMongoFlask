# Extrator de Artigos da Constituição Brasileira de 1988
## Aplicação com o propósito de localizar, extrair e armazenar os artigos de leis da constituição brasileira de 1988 a partir de um PDF.

## Sistema desenvolvido em Python, utilizando Flask como framework web, MongoDB como banco de dados e o Docker para a disponibilização dos serviços.

Para utilização da aplicação é necessário:

instalar o docker seguindo:

    https://docs.docker.com/engine/install/ubuntu/

Depois, clonar o repositório:

    git clone git@gitlab.com:cleberfeijo/DockerizedMongoFlask.git

Para iniciar a aplicação, abra o terminal no diretorio onde clonou os arquivos e use:

    docker-compose up -d
(-d para tudo ser feito em segundo plano)

Depois de instalado basta acessar a api:

    http://localhost:80

Para acessar a api de extração de artigos:

    http://localhost:80/extrair
(Deve demorar um tempinho para todos os artigos serem extraídos e armazenados, você será avisado quando acabar!)

Para acessar a api para checar os artigos no banco de dados:

    http://localhost:80/artigos

Para parar a aplicação:

    docker-compose down

Caso queira parar a aplicação e remover as imagens dos containers para alterar algo no código use:

    docker-compose down --rmi all




